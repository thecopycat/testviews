package com.example.androidtestview.gui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug.CapturedViewProperty;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.androidtestview.R;

public class EditableTextView extends LinearLayout {

	private static final String TAG = EditableTextView.class.getSimpleName();
	
	private TextView textView;
	private EditText editText;
	
	private EditableTextListener editListener;
	
	boolean editmode = false;
	
	public EditableTextView(Context context) {
		super(context);
		init(context,null);
	}
	
	public EditableTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context,attrs);
	}
	
	public EditableTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context,attrs);
	}

	public void init(Context context,AttributeSet attrs) {
		LayoutInflater.from(context).inflate(R.layout.editable_text_layout, this);
		editText= (EditText) findViewById(R.id.editText);
		textView = (TextView)findViewById(R.id.textView);
		findViewById(R.id.edit_icon).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switchMode();
				
			}
		});
		if (attrs != null) {
			for (int i=0;i<attrs.getAttributeCount();i++) {
				Log.d(TAG,"attribute "+i+" : "+attrs.getAttributeName(i)+" -"+attrs.getAttributeValue(i));
			}
			String text = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "text");
			textView.setText(text);
		}
	}
	
	public void switchMode() {
	 if (editmode) switchToReadMode();
	 else switchToEditMode();
	}
	
	public void switchToEditMode() {
		editmode=true;
		textView.setVisibility(GONE);
		editText.setVisibility(VISIBLE);
		editText.setText(textView.getText().toString());
		editText.requestFocus();
		editText.setSelection(editText.getText().length());
	}
	
	public void switchToReadMode() {
		editmode=false;
		textView.setVisibility(VISIBLE);
		editText.setVisibility(GONE);
		//toString is called to remove any spannable (underline, etc)
		textView.setText(editText.getText().toString());
		if (editListener != null) {
			editListener.onTextChanged(textView.getText().toString());
		}
	}
	
	@CapturedViewProperty
	public CharSequence getText() {
		if (editmode) {
			return editText.getText();
		} else {
			return textView.getText();
		}
	}
	
	public void setText(CharSequence s) {
		if (editmode) {
			editText.setText(s);
		} else {
			textView.setText(s);
		}
	}
	
	public void setEditableTextListener(EditableTextListener listener) {
		this.editListener = listener;
	}
	
	public static interface EditableTextListener {
		public void onTextChanged(String newValue);
	}
	
}
