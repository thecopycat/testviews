package com.example.androidtestview.gui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.androidtestview.R;

public class ExpandView extends RelativeLayout implements OnClickListener{

	private static final String TAG = ExpandView.class.getSimpleName();
	
	
	private LinearLayout customTitle;
	private TextView title;
	private ImageView button;
	private ViewGroup content;
		
	public ExpandView(Context context) {
		super(context);
		init(context,null);
	}
	
	public ExpandView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context,attrs);
	}
	
	public ExpandView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context,attrs);
	}
	
	private void init(Context context, AttributeSet attrs) {
		Log.d(TAG,"beginning expand view initialization");
		LayoutInflater inflater = LayoutInflater.from(context);

		inflater.inflate(R.layout.expand_view_layout, this);
		title = (TextView) findViewById(R.id.ev_title);
		button = (ImageView) findViewById(R.id.ev_image);

		content = (ViewGroup) findViewById(R.id.ev_content);
		customTitle = (LinearLayout)findViewById(R.id.ev_custom_title);
		title.setOnClickListener(this);
		button.setOnClickListener(this);
		if (attrs !=null) {
			TypedArray array = context.obtainStyledAttributes(attrs,R.styleable.ExpandView_LayoutParams);
			boolean expand = array.getBoolean(R.styleable.ExpandView_LayoutParams_opened, false);
			if (expand) onClick(null);
		}
		Log.d(TAG,"finished expand View initialization");
			
			
			
	}
	
	public void setTitle(View customTitleView) {
		title.setVisibility(View.GONE);
		customTitle.setVisibility(View.VISIBLE);
		customTitle.addView(customTitleView);
	}
	
	public void setTitle(String titleText) {
		customTitle.setVisibility(View.GONE);
		customTitle.removeAllViews();
		
		title.setVisibility(View.VISIBLE);
		title.setText(titleText);
	}

	
	@Override
	public void addView(View child) {
		if (content!=null) 
			content.addView(child);
		else
			super.addView(child);
			
	}
	
	@Override
	public void addView(View child, int index) {
		if (content!=null) 
			content.addView(child, index);
		else
			super.addView(child, index);
	}
	
	
	@Override
	public void addView(View child, int index,
			android.view.ViewGroup.LayoutParams params) {
		if (content!=null) 
			content.addView(child, index, params);
		else
			super.addView(child, index, params);
	}
	
	@Override
	public void addView(View child, int width, int height) {
		if (content!=null) 
			content.addView(child, width, height);
		else
			super.addView(child, width, height);
	}
	
	@Override
	public void addView(View child, android.view.ViewGroup.LayoutParams params) {
		if (content!=null) 
			content.addView(child, params);
		else
			super.addView(child, params);
	}
	
	
	@Override
	public void onClick(View v) {
		if (content.getVisibility()==View.GONE) {
			content.setVisibility(View.VISIBLE);
			button.setImageResource(R.drawable.down_arrow);
		
		} else {
			content.setVisibility(View.GONE);
			button.setImageResource(R.drawable.right_arrow);
		}
	}
	
	

}
