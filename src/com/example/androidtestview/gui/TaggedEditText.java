package com.example.androidtestview.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.androidtestview.R;

public class TaggedEditText extends EditText {
	
	private static final String TAG = TaggedEditText.class.getSimpleName();

	private Context mContext;
	
	public TextView createContactTextView(String text){
		TextView tv = new TextView(mContext);
		tv.setText(text);
		tv.setBackgroundResource(R.color.bubble);
		tv.setCompoundDrawablesWithIntrinsicBounds(0, 0,android.R.drawable.presence_offline, 0);
		return tv;
	}
	
	public TaggedEditText(Context context) {
		super(context);
		init(context);
	}
	
	public TaggedEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public TaggedEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	public void init(Context context) {
		this.mContext = context;
		TextWatcher watcher = new TextWatcher() {
			
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() > 0 && ' ' == s.charAt(s.length()-1)){
					Log.d(TAG,"Sequence : \""+s+"\"");
					removeTextChangedListener(this);

					String[] contacts = s.toString().split(" "); 

					SpannableStringBuilder sb = new SpannableStringBuilder();
					for(String contact : contacts){
						sb.append(getSpannableString(contact));
						sb.append(" ");
						
					}
					setText(sb);
					setSelection(getText().length());

					addTextChangedListener(this);
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		};
		addTextChangedListener(watcher);
	}
	
	public SpannableStringBuilder getSpannableString(String tagName) {
		final SpannableStringBuilder sb = new SpannableStringBuilder();
		TextView tv = createContactTextView(tagName);
		BitmapDrawable bd = (BitmapDrawable) convertViewToDrawable(tv);
		bd.setBounds(0, 0, bd.getIntrinsicWidth(),bd.getIntrinsicHeight()); 
		sb.append(tagName);
		sb.setSpan(new ImageSpan(bd), 0,(tagName.length()),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		setMovementMethod(LinkMovementMethod.getInstance());
		ClickableSpan clickSpan = new ClickableSpan() {

			@Override
			public void onClick(View view) { 
				Log.v("clicked", view.getClass().getSimpleName());

				int i = ((EditText) view).getSelectionStart();
				int j = ((EditText) view).getSelectionEnd();
				getText().replace(Math.min(i, j ),
						Math.max(i, j ), "", 0, "".length());

			}

		};
		sb.setSpan(clickSpan, sb.length() - tagName.length(),
				sb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return sb;
	}
	
	public BitmapDrawable convertViewToDrawable(View view) {
		int spec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		view.measure(spec, spec);
		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
		Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
				Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		c.translate(-view.getScrollX(), -view.getScrollY());
		view.draw(c);
		view.setDrawingCacheEnabled(true);
		Bitmap cacheBmp = view.getDrawingCache();
		Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true);
		view.destroyDrawingCache();
		return new BitmapDrawable(mContext.getResources(),viewBmp);

	}

}
